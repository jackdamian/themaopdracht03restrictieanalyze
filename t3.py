#!/usr/bin/env python3
"""
Dit is deelopdracht 4 van thema 3
"""


# imports
import argparse
import re
import sys


# metadata variables
__author__ = 'Jack de Boer'
__version__ = '2017.31.03'


# global variables
ENZYM_DICT = {"EcorI": ["GAATTC", 1], "BamHI": ["GGATCC", 1], "EcoRII": ["CCWGG", 0],
              "HindIII":  ["AAGCTT", 1], "TaqI": ["TCGA", 1], "NotI": ["GCGGCCGC", 2],
              "HinfI": ["GANTCA", 1], "Sau3A": ["GATC", 0], "HaeIII": ["GGCC", 2],
              "EcoRV": ["GATATC", 3], "PstI": ["CTGCAG", 4], "XbaI": ["TCTAGA", 1],
              "MboI": ["GATC", 0], "mst2": ["CCTNAGG", 2]}


# objects
class RestrictieEnzym:
    """"Een RestrictieEnzym bestaat uit een naam herkenningsequentie en kniplaats
    met de deze data kan he enzym DNA sequenties in stukken knippen."""
    def __init__(self, naam, herkenningsequentie, knipplaats):
        self.naam = naam
        self.herkenningsequentie = herkenningsequentie
        self.knipplaats = knipplaats

    def verwerk_ambigious_nucleotides(self):
        """"Vervangt de ambigious nucleotides in de herkenningsequentie in een versie
        die werkt met regular expression."""
        w_nucleotide = re.compile('W')
        nieuwe_sequentie = w_nucleotide.sub("[AT]", self.herkenningsequentie)
        n_nucleotide = re.compile('N')
        nieuwe_sequentie = n_nucleotide.sub("[ATCG]", nieuwe_sequentie)

        return nieuwe_sequentie

    def vind_knip_positie(self, fragment):
        """Via regular expression word gevonden waar de herkenningsequentie in het
        fragment voorkomt hieraan wordt ook de knipplaats toegevoegd om zo de exacte
        knip positie te hebben."""
        herken_sequentie = re.compile(self.verwerk_ambigious_nucleotides())
        knip_posities = []

        for match_object in herken_sequentie.finditer(fragment):
            knip_posities.append(match_object.start() + self.knipplaats)
        knip_posities.reverse()

        return knip_posities

    def knip_fragmenten(self, fragmenten):
        """Knipt de fragmenten op de juiste knipposities. Als er geen knipposities zijn
         worden de fragmenten zonder aanpassing gereturned"""
        nieuw_fragment = []

        for fragment in fragmenten:
            knip_posities = self.vind_knip_positie(fragment)
            if knip_posities > [0]:
                for knip_positie in knip_posities:
                    nieuw_fragment.append(fragment[knip_positie:])
                    fragment = fragment[:knip_positie]
                nieuw_fragment.append(fragment[:knip_positie])
            else:
                nieuw_fragment.append(fragment)

        return nieuw_fragment


class FastaSequentie:
    """Een fasta sequentie bestaat uit een header en de bijbehorende sequentie"""
    def __init__(self, header, fragmenten):
        self.header = header
        self.fragmenten = fragmenten


# functions
def parsecommandline():
    """Gebruikt argparse om benodigde data van de CLI te halen."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--sequenceFile", help="Het pad naar de fasta file", required=True)
    parser.add_argument("-e", "--Enzym", help="Een restrictie enzym om mee te knippen."
                                              "Als geen gegeven wordt MboI gebruikt", default='MboI', nargs='*')
    args = parser.parse_args()

    if args.sequenceFile:
        dna_path = args.sequenceFile
    if args.Enzym:
        enzymen = args.Enzym
    else:
        parser.print_help()
        sys.exit()

    return [dna_path, enzymen]


def get_fasta(data):
    """"Valideert pad van multifasta file en maakt daarvan een list van FastaSequenties."""
    fasta_file = data
    fasta_list = []
    fragmenten = []

    try:
        with open(fasta_file, "r") as fasta:
            for line in fasta:
                line = line.strip()
                if line.startswith(">"):
                    if fragmenten:
                        fasta_list.append(FastaSequentie(header, fragmenten))
                    header = line.strip(">")
                    fragmenten = []
                else:
                    fragmenten.append(line)
            fasta_list.append(FastaSequentie(header, fragmenten))

    except FileNotFoundError:
        print("usage: T3_DO3.py [-h] -s SEQUENCEFILE [-e [ENZYM [ENZYM ...]]]"
              "\nt3.py: error: argument -s/--sequenceFile: sequencefile invalid")
        sys.exit()

    return fasta_list


def get_enzymen(data):
    """"Leest welke enzymen gevraagd zijn uit CLI en maakt
     een list van RestrictieEnzym met die namen."""
    enzym_namen = data
    enzym_list = []

    try:
        for naam in enzym_namen:
            if enzym_namen == "MboI":
                enzym_data = ENZYM_DICT["MboI"]
            else:
                enzym_data = ENZYM_DICT[naam]
            enzym_list.append(RestrictieEnzym(naam, enzym_data[0], enzym_data[1]))

    except KeyError:
        print("usage: T3_DO3.py [-h] -s SEQUENCEFILE [-e [ENZYM [ENZYM ...]]]"
              "\nt3.py: error: argument -e/--enzyme: enzyme name not recognized")
        sys.exit()

    return enzym_list


def knip_sequenties_met_enzymen(fasta_list, enzym_list):
    """"Knipt alle gegeven FastaSequenties met alle gegeven RestrictieEnzymen.
    met de ontstane fragmenten wordt een nieuwe fasta list gemaakt"""
    nieuwe_fasta = []

    for fasta in fasta_list:
        for enzym in enzym_list:
            nieuwe_fragmenten = enzym.knip_fragmenten(fasta.fragmenten)
            if nieuwe_fragmenten != fasta.fragmenten:
                fasta = FastaSequentie(fasta.header, nieuwe_fragmenten)
        nieuwe_fasta.append(FastaSequentie(fasta.header, nieuwe_fragmenten))

    return nieuwe_fasta


def get_molecuul_gewicht(sequentie):
    """"Teld per nucleotide het gewicht (in dalton) op door via map het gewicht van elke letter
    van de sequentie te geven van de dictionary, dit wordt dan via sum bijelkaar opgeteld."""
    nucleotide_gewicht = {'T': 321.21, 'G': 346.22, 'A': 330.22, 'C': 306.19}
    molecuul_gewicht = sum(map(lambda x: nucleotide_gewicht[x], sequentie))

    return round(molecuul_gewicht, 2)


def sorteer_op_molecuul_gewicht(fragmenten):
    """gebruikt get_molecuul_gewicht om het molecuul gewicht te berekenen van
    alle sequenties in de gegeven fragmenten. de molecuul gewichten worden
    gesorteerd en gereturned"""
    molecuul_gewichten = []

    for sequentie in fragmenten:
        molecuul_gewichten.append(get_molecuul_gewicht(sequentie))
    molecuul_gewichten.sort()

    return molecuul_gewichten


def performRestriction(dna_path, enzymen):
    """Voert de gehele restrictie uit."""
    resultaat_dict = {}
    fasta_list = get_fasta(dna_path)
    enzym_list = get_enzymen(enzymen)
    verwerkte_fasta_list = knip_sequenties_met_enzymen(fasta_list, enzym_list)

    for fasta in verwerkte_fasta_list:
        gesorteerde_gewichten = sorteer_op_molecuul_gewicht(fasta.fragmenten)
        resultaat_dict[fasta.header] = gesorteerde_gewichten

    return resultaat_dict


def print_resultaten(resultaten):
    """print de resultaten van de andere functies"""
    print("Massa's van ontstane fragmenten:\n", resultaten)


# main
def main():
    """Roept alle functies op."""
    data = parsecommandline()
    resultaten = performRestriction(data[0], data[1])
    print_resultaten(resultaten)


if __name__ == '__main__':
    sys.exit(main())
